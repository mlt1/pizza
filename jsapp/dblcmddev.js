/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//Choix de la formule  pour la commande
$(document).ready(function () {

    $("#form-tarif").submit(function (e) {
        e.preventDefault();
        var configformulecmd = $('input[name=regcmd]:checked').val();
        var formulecmd = $('select[name=formule]').val();
        var emailguest = $('#emailguest').val();
        var campagne = $('input[name=campagne]').val();


        if (emailguest) {
            var params = {
                "emailguest": emailguest,
            };
            $.ajax({
                url: BASEURL + 'Appdev/validateguest',
                type: 'POST', // type of the HTTP request
                dataType: 'json',
                data: params,
                success: function (data) {
                    if (data.success) {
                        var params = {
                            "configformulecmd": configformulecmd,
                            "formulecmd": formulecmd,
                            "campagne": campagne,
                        };
                        $.ajax({
                            url: BASEURL + 'Appdev/configstartcmd/',
                            type: 'POST', // type of the HTTP request
                            data: params,
                            dataType: 'json',
                            success: function (res) {
                                if (res.errormsg) {
                                    swal("Oh non!", res.errormsg, "error");
                                } else {
                                    swal("Confirmer Votre Email !", "Vous pouvez continuer votre projet mais votre compte n'est pas encore confirmé. Vérifiez votre email s'il vous plait ", "warning");
                                    $(".swal-button").click(function (e) {

                                        $('#ModalFT').modal('hide');
                                        location.reload();
                                    });
                                }
                            }
                        });

                    } else {
                        swal("Oh non!", data.errormsg, "error");
                    }
                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });
        } else {
            var params = {
                "configformulecmd": configformulecmd,
                "formulecmd": formulecmd,
                "campagne": campagne,
            };
            $.ajax({
                url: BASEURL + 'Appdev/configstartcmd/',
                type: 'POST', // type of the HTTP request
                data: params,
                dataType: 'json',
                success: function (res) {
                    if (res.errormsg) {
                        swal("Oh non!", res.errormsg, "error");
                    } else {
                        swal("Choix a été enregistré", "Configuration a été effectuée avec succès!", "success");
                        $(".swal-button").click(function (e) {

                            $('#ModalFT').modal('hide');
                            location.reload();
                        });
                    }

                }
            });
        }


    })


//****Chek BOX GCV
    $('input[name="cgv"]').click(function () {
        if ($(this).prop("checked") == true) {
            $.ajax({
                url: BASEURL + 'Dbl/cmdmethodepayementok/',
                type: 'GET', // type of the HTTP request
                success: function (response) {

                    //      alert(response);
                }
            });
        } else if ($(this).prop("checked") == false) {
            $.ajax({
                url: BASEURL + 'Dbl/cmdmethodepayementnotok/',
                type: 'GET', // type of the HTTP request
                success: function (response) {
                    // alert(response);
                }
            });
        }
    });


})
;

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

function modifmp() {
    $(".affichedetailmp").hide();
    $(".choixmp").show();
}

function chachebtn() {
    $(".confirmstepcmd").attr("readonly", true);
    document.getElementsByClassName("confirmstepcmd").disabled = true;
    document.getElementsByClassName("confirmstepcmd").addClass('disabled');
}

//Calcule de la commande en temps réel

//Save Méthode de  livraison

//réserver un quartier
function reservquartier(code_iris) {
    $(".gm-style-iw-t").hide();
    $.ajax({
        url: BASEURL + 'Dbl/reserver_quartier/' + code_iris,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            if (data == 0) {

                swal("Oh non!", "Vous avez déjà réservé ce quartier", "error");
            } else {
                quartierslist();// fill districts table
                communeslistsajax(); // fill commmons list
                pricelivraison(); // fill delivry
                totalstartcmd(); // fill left side bar
                snglquartierreserv(code_iris);   //contient les quartier réservié sur le map
                notificationtost();

            }

        }
    });

}

$("#loader").hide(); //loader s'affiche quand le client commencer la réservation d'une commune
//réserver la totalite d'une comune
function reservcommune(insee_com) {
    $(".gm-style-iw-t").hide();
    $("#map").hide();
    $("#loader").show();
    $.ajax({
        url: BASEURL + 'Appdev/count_quartiers_commune/' + insee_com,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            loading(parseInt(data));
            $.ajax({
                url: BASEURL + 'Appdev/reserver_quartiers_commune/' + insee_com,
                type: 'GET', // type of the HTTP request
                success: function (data) {
                    if (data == 0) {
                        swal("Oh non!", "Vous avez déjà réservé cette commune", "error");
                    } else {
                        allquartierreservsgncom(insee_com);
                        quartierslist(); // fill districts table
                        communeslistsajax(); // fill commmons tables
                        pricelivraison(); // fill delivry price
                        totalstartcmd(); // fill left side bar
                        notificationtostcommune();
                        $("#loader").hide();
                        $("#map").show();
                    }

                }
            });
        }
    });


}

//GET INFOS  COMMUNE DEPUIS LA BASE DE DONNEES
function getinfoscommune(codeinse) {

    $.ajax({
        url: BASEURL + 'Dbl/updatemethodelivraison/',
        type: 'GET', // type of the HTTP request
        success: function (data) {

            $('#methodelivraisonsaved').html(data);


        }
    });
}

//Gestion des tab
function opentype(evt, cityName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("type");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " w3-red";
}

function activatesearche() {
    var input_search = document.getElementById('pac-input');

    var autocomplete = new google.maps.places.Autocomplete(input_search);
    autocomplete.bindTo('bounds', map);

    autocomplete.setFields(
        ['address_components', 'geometry', 'icon', 'name']);
    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function () {
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setPosition(place.geometry.location);
        marker.getPosition();

        //get commune

        marker.setVisible(true);
        getgeojsonbylocation(marker.getPosition().lat(), marker.getPosition().lng())

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindowContent.children['place-icon'].src = place.icon;
        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-address'].textContent = address;
        infowindow.open(map, marker);


        document.getElementById('use-strict-bounds')
            .addEventListener('click', function () {
                console.log('Checkbox clicked! New state=' + this.checked);
                autocomplete.setOptions({strictBounds: this.checked});
            });
    });
}


var map;
var marker;
var mapbuttons;

//Chargement de la map
function initMap() {
    //var option ='cartefrance.json';
    var option = 'departements.json';

    //Declaration de la map

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5.8,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: 47, lng: 3.34,} //egion-Nouvelle-Aquitaine
    });
    $('#map').append("<div id='topmap'><button id='alldepartement' class=\"btn btn-warning\" style=\"background-color: #4cae4c; border-color: #4cae4c;margin: 20px;\"><i class=\"fa fa-arrow-circle-left\" style=\"font-size:16px;\" aria-hidden=\"true\"></i>Tous les départements</button></div><div class = 'input-field' id='searchdiv'> <input id='pac-input' class='form-control' placeholder='Adresse de mon business' type='text' size='50'><div class='icon-wrap' style='position: absolute;right: 4%; top:31px;'><svg class='bi bi-search' width='1em' height='1em' color='#ccc' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>\n" +
        "  <path fill-rule='evenodd' d='M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z' clip-rule='evenodd'/>\n" +
        "  <path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z' clip-rule='evenodd'/>\n" +
        "</svg></div></div>");


    activatesearche()


    infoWindow = new google.maps.InfoWindow({
        content: ""
    });

    quartierslist();
    communeslistsajax();

    map.data.map.data.loadGeoJson(BASEURL + 'repo/' + option);


    map.data.setStyle({
        fillColor: 'green',
        strokeWeight: 1
    });


    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    map.data.addListener('click', function (event) {
        //show an infowindow on click
        infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom") + "\</h6>   <br><center><button onclick='getCOMJSON(" + event.feature.getProperty("code") + ")' class='btn btn-primary'>Les communes</button></center></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });
    $("#alldepartement").click(function () {
        initMap()
    });

}

function initMap2() {
    //var option ='cartefrance.json';

    //Declaration de la map

    map2 = new google.maps.Map(document.getElementById('map'), {
        zoom: 5.8,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: 47, lng: 3.34,} //egion-Nouvelle-Aquitaine
    });


}

//Les departements
function lesdeppartements(option) {
    //var option ='cartefrance.json';
    //Declaration de la map
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: 44.785664, lng: -2.427603,} //egion-Nouvelle-Aquitaine
    });


    map.data.loadGeoJson(
        BASEURL + 'repo/' + option);
    map.data.setStyle({
        fillColor: 'green',
        strokeWeight: 1
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    map.data.addListener('click', function (event) {
        //show an infowindow on click
        //<button onclick='getdepJSON("+event.feature.getProperty("code")+")' class='btn btnmr btn-success'>Réserver</button> En ca de besoin la reservation d'une Departement entier
        infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom") + "\</h6>   <br><center><button onclick='getCOMJSON(" + event.feature.getProperty("code") + ")' class='btn btn-primary'>Les communes</button></center></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });
    $('#map').append("<div id='topmap'><button id='alldepartement' class=\"btn btn-warning\" style=\"background-color: #4cae4c; border-color: #4cae4c;margin: 20px;\"><i class=\"fa fa-arrow-circle-left\" style=\"font-size:16px;\" aria-hidden=\"true\"></i>Tous les départements</button></div><div class = 'input-field' id='searchdiv'> <input id='pac-input' class='form-control' placeholder='Adresse de mon business' type='text' size='50'><div class='icon-wrap' style='position: absolute;right: 4%; top:31px;'><svg class='bi bi-search' width='1em' height='1em' color='#ccc' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>\n" +
        "  <path fill-rule='evenodd' d='M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z' clip-rule='evenodd'/>\n" +
        "  <path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z' clip-rule='evenodd'/>\n" +
        "</svg></div></div>");
    $("#alldepartement").click(function () {
        initMap()
    });
}

function communefromfilter(option) {

    //Declaration de la map
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: 44.785664, lng: -2.427603,} //egion-Nouvelle-Aquitaine
    });


    map.data.loadGeoJson(
        BASEURL + '' + option);
    map.data.setStyle({
        fillColor: 'green',
        strokeWeight: 1
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    map.data.addListener('click', function (event) {
        //show an infowindow on click
        infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom") + "\</h6>   <br><center><button onclick='getCOMJSON(" + event.feature.getProperty("code") + ")' class='btn btn-danger'>Les communes</button> <button onclick='getdepJSON(" + event.feature.getProperty("code") + ")' class='btn btnmr btn-success'>Réserver</button><button onclick='initMap(\"departements.json\")' class='btn btnmr btn-info'>Zoom</button></center></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });
    $('#map').append("<div id='topmap'><button id='alldepartement' class=\"btn btn-warning\" style=\"background-color: #4cae4c; border-color: #4cae4c;margin: 20px;\"><i class=\"fa fa-arrow-circle-left\" style=\"font-size:16px;\" aria-hidden=\"true\"></i>Tous les départements</button></div><div class = 'input-field' id='searchdiv'> <input id='pac-input' class='form-control' placeholder='Adresse de mon business' type='text' size='50'><div class='icon-wrap' style='position: absolute;right: 4%; top:31px;'><svg class='bi bi-search' width='1em' height='1em' color='#ccc' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>\n" +
        "  <path fill-rule='evenodd' d='M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z' clip-rule='evenodd'/>\n" +
        "  <path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z' clip-rule='evenodd'/>\n" +
        "</svg></div></div>");
    $("#alldepartement").click(function () {
        initMap()
    });
}

//Select One departement
function getdepJSON(option) {
    $.ajax({
        url: BASEURL + 'Tools/depjson/' + option,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            // Call function zoom departement
            zoomdepartement(data);

        }
    });
}

//Select One departement
function getCOMJSON(option) {
    $.ajax({
        url: BASEURL + 'Tools/communejson/' + option,
        type: 'GET', // type of the HTTP request
        success: function (data) {

            //Call function depcommunes pour afficher les communes
            obj = JSON.parse(data);
            //console.log(obj);
            var lng = obj.longitude;
            var lat = obj.latitude;
            var option = obj.communes;
            depcommunes(option, lat, lng);
        }
    });
}

//Affficher une commune sans quartier
//Select One commune
function getSNGLCOMJSON(option) {
    $.ajax({
        url: BASEURL + 'Tools/signlecommunejson/' + option,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            obj = JSON.parse(data);
            //console.log(obj);
            var lng = obj.lng;
            var lat = obj.lat;
            var option = obj.fichiercom;
            //   alert(option);
            //Call function depcommunes pour afficher les communes
            signlecommune(option, lat, lng);
        }
    });
}

//Affficher une commune avec quartier
//Select One commune
function getcomquart(option) {
    $.ajax({
        url: BASEURL + 'Tools/signlecommunequartierjson/' + option,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            //Call function depcommunes pour afficher les communes
            obj = JSON.parse(data);
            //console.log(obj);
            var lng = obj.lng;
            var lat = obj.lat;
            var option = obj.fichierquartier;
            //   var option =  'repo/departements/75-Paris/communesbyquartiers/Paris.geojson';
            //alert(option);
            signlecomquartier(option, lat, lng);
        }
    });


    //  $('#map').append("<div id='topmap'><button id='alldepartement' class=\"btn btn-primary\"><i class=\"fa fa-arrow-circle-left\" style=\"font-size:16px;\" aria-hidden=\"true\"></i>Tous les départements</button></div><div class = 'input-field' id='searchdiv'> <input id='pac-input' class='form-control' placeholder='Adresse de mon business' type='text' size='50'><div class='icon-wrap' style='position: absolute;right: 4%; top:31px;'><svg class='bi bi-search' width='1em' height='1em' color='#ccc' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>\n" +
    //     "  <path fill-rule='evenodd' d='M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z' clip-rule='evenodd'/>\n" +
    //     "  <path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z' clip-rule='evenodd'/>\n" +
    //     "</svg></div></div>");
    // $("#alldepartement").click(function () {
    //    initMap()
    //});

}


function getgeojsonbylocation(lat, lng) {
    $.ajax({
        url: BASEURL + 'Tools/geojsonbylocation/' + lat + '/' + lng,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            //Call function depcommunes pour afficher les communes
            obj = JSON.parse(data);
            //console.log(obj);
            var lng = obj.lng;
            var lat = obj.lat;
            var option = obj.fichierquartier;
            //   var option =  'repo/departements/75-Paris/communesbyquartiers/Paris.geojson';
            //alert(option);
            signlecomquartier(option, lat, lng);
        }
    });
}

function getsinglquartier(option) {

    // alert(option);
    $.ajax({
        url: BASEURL + 'Tools/signlequartierjson/' + option,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            //Call function depcommunes pour afficher les communes
            obj = JSON.parse(data);
            var lng = obj.lng;
            var lat = obj.lat;
            var option = obj.fichier;
            signlequartier(option, lat, lng);
        }
    });
}

//Faire un Zoom sur une departement
function zoomdepartement(option) {

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 9,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: 46.603354, lng: 1.888334,} //egion-Nouvelle-Aquitaine
    });
    map.data.loadGeoJson(
        BASEURL + 'repo/' + option);
    map.data.setStyle({
        fillColor: 'tan',
        strokeWeight: 1
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    map.data.addListener('click', function (event) {
        //show an infowindow on click
        infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom_iris") + "\</h6> <h5>" + event.feature.getProperty("nom_com") + "</h5> <h6> Population : " + event.feature.getProperty("p12_pop") + "<hr /></h6>   <br><center><button onclick='getCOMJSON(" + event.feature.getProperty("code_dept") + ")' class='btn btnmr btn-success'>Communes</button> <button onclick='getdepJSON(" + event.feature.getProperty("code") + ")' class='btn btnmr btn-success'>Réserver</button><button onclick='getSNGLCOMJSON(" + event.feature.getProperty("insee_com") + ")' class='btn btn-info'>Zoom</button></center></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });

    $('#map').append("<div id='topmap'><button id='alldepartement' class=\"btn btn-warning\" style=\"background-color: #4cae4c; border-color: #4cae4c;margin: 20px;\"><i class=\"fa fa-arrow-circle-left\" style=\"font-size:16px;\" aria-hidden=\"true\"></i>Tous les départements</button></div><div class = 'input-field' id='searchdiv'> <input id='pac-input' class='form-control' placeholder='Adresse de mon business' type='text' size='50'><div class='icon-wrap' style='position: absolute;right: 4%; top:31px;'><svg class='bi bi-search' width='1em' height='1em' color='#ccc' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>\n" +
        "  <path fill-rule='evenodd' d='M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z' clip-rule='evenodd'/>\n" +
        "  <path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z' clip-rule='evenodd'/>\n" +
        "</svg></div></div>");
    $("#alldepartement").click(function () {
        initMap()
    });
}

//Faire un Zoom sur une departement
function depcommunes(option, lat, lng) {
    var lat = parseFloat(lat);
    var lng = parseFloat(lng);

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 9,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: lat, lng: lng,} //egion-Nouvelle-Aquitaine
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    var data_layer = new google.maps.Data({map: map});
    //récuperer tous les fichier GeoJSON  pour les articles réserver
    $.ajax({
        url: BASEURL + 'Dbl/allfilequartierreserved/',
        type: 'GET', // type of the HTTP request
        success: function (response) {
            //  var string = JSON.stringify(response)
            var objects = JSON.parse(response)

            //Call function depcommunes pour afficher les communes
            // obj =  JSON.parse(response);
            //console.log(obj);

            objects.forEach(function (key, index) {


                data_layer.loadGeoJson(BASEURL + key.fichier);
                data_layer.setStyle({
                    fillColor: 'red',
                    strokeWeight: 1,
                    fillOpacity: 0.35
                });
            })

        }
    });
    data_layer.addListener('click', function (event) {
        //show an infowindow on click
        infosdetailcommune(event.feature.getProperty("insee_com"));
        var status = event.feature.getProperty("statut");
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="initMap(\'departements.json\')" class="btn btnmr btn-warning" style="display: none"><i class="fa fa-arrow-circle-left" style="font-size:16px; " aria-hidden="true"></i>départements</button>  <button onclick="getcomquart(' + event.feature.getProperty("insee_com") +  '\')"class="btn btnmr margin-left btn-warning"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i>les Quartiers de la commune ' + event.feature.getProperty("nom_com") + '</button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dept") + "\</h5> <h5 class='titlecont'>" + event.feature.getProperty("nom_com") + "</h5> <div id='infoscommune'><hr></div> <center><div><h5 style='color: red'> Cette commune est déjà réservée</h5><button onclick='removecommune(" + event.feature.getProperty("insee_com") + ")' class='btn btnmr btn-warning'><i class='fa fa-trash' style='font-size:16px;' aria-hidden='true'></i>supprimer cette commune</button></div></center></div></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);

    });


    map.data.loadGeoJson(
        BASEURL + 'repo/' + option);
    map.data.setStyle({
        fillColor: 'blue',
        strokeWeight: 1
    });

    map.data.addListener('click', function (event) {
        //show an infowindow on click
        infosdetailcommune(event.feature.getProperty("insee_com"));
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="initMap(\'departements.json\')" class="btn btnmr btn-warning" style="display: none"><i class="fa fa-arrow-circle-left" style="font-size:16px;" aria-hidden="true"></i>départements</button> <button onclick="getcomquart(' + event.feature.getProperty("insee_com") + ')"class="btn btnmr margin-left btn-warning"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i> les Quartiers de la commune ' + event.feature.getProperty("nom_com") + ' </button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dep") + "\</h5> <h5 class='titlecont'>" + event.feature.getProperty("nom_com") + "</h5> <div id='infoscommune'><hr></div>  </div><div style='margin-bottom=10px;'><center><a href='#'  onclick='reservcommune(" + event.feature.getProperty("insee_com") + ")' class='btn btnmr btn-success btninfowindow' title='Réserver une commune '><i class='fa fa-shopping-cart'></i><i  style='font-style: normal;'> Réserver la commune(tous les quartiers)</i></a></center></div></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });

    marker.bindTo('map', data_layer);


    $('#map').append("<div id='topmap'><button id='alldepartement' class=\"btn btn-warning\" style=\"background-color: #4cae4c; border-color: #4cae4c;margin: 20px;\"><i class=\"fa fa-arrow-circle-left\" style=\"font-size:16px;\" aria-hidden=\"true\"></i>Tous les départements</button></div><div class = 'input-field' id='searchdiv'> <input id='pac-input' class='form-control' placeholder='Adresse de mon business' type='text' size='50'><div class='icon-wrap' style='position: absolute;right: 4%; top:31px;'><svg class='bi bi-search' width='1em' height='1em' color='#ccc' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>\n" +
        "  <path fill-rule='evenodd' d='M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z' clip-rule='evenodd'/>\n" +
        "  <path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z' clip-rule='evenodd'/>\n" +
        "</svg></div></div>");
    $("#alldepartement").click(function () {
        initMap()
    });
    activatesearche();

}

//Faire un Zoom sur une commune sans quartier
function signlecommune(option, lat, lng) {

    var lat = parseFloat(lat);
    var lng = parseFloat(lng);
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12.5,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: lat, lng: lng,} //egion-Nouvelle-Aquitaine
    });
    map.data.loadGeoJson(
        BASEURL + '' + option);

    $('#map').append("<div id='topmap'><button id='alldepartement' class=\"btn btn-warning\" style=\"background-color: #4cae4c; border-color: #4cae4c;margin: 20px;\"><i class=\"fa fa-arrow-circle-left\" style=\"font-size:16px;\" aria-hidden=\"true\"></i>Tous les départements</button></div><div class = 'input-field' id='searchdiv'> <input id='pac-input' class='form-control' placeholder='Adresse de mon business' type='text' size='50'><div class='icon-wrap' style='position: absolute;right: 4%; top:31px;'><svg class='bi bi-search' width='1em' height='1em' color='#ccc' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>\n" +
        "  <path fill-rule='evenodd' d='M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z' clip-rule='evenodd'/>\n" +
        "  <path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z' clip-rule='evenodd'/>\n" +
        "</svg></div></div>");
    btn_back_to_dep = "<button id = 'backdep' style = 'margin:20px;' onclick=\"getCOMJSON('"
    btn_back_to_dep = btn_back_to_dep.concat("')\" class='btn btn-warning'><i class='fa fa-arrow-circle-left' style='font-size:16px;' aria-hidden='true'></i> toutes les communes de ")
    btn_back_to_dep = btn_back_to_dep.concat("</button>")
    $('#topmap').append(btn_back_to_dep);


    map.data.setStyle(function (feature) {
        code_dept = feature.getProperty('insee_dep');
        nom_dept = feature.getProperty('nom_dep');
        $("#backdep").attr('onclick', 'getCOMJSON('+code_dept+')')
        $("#backdep").html("<i class='fa fa-arrow-circle-left' style='font-size:16px;' aria-hidden='true'></i>toutes les communes de "+nom_dept)
        return ({
            fillColor: 'orange',
            strokeWeight: 1
        });
    });


    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    map.data.addListener('click', function (event) {
        //show an infowindow on click
        //infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom_dep") + "\</h6> <h5>"+ event.feature.getProperty("nom_com") +":"+ event.feature.getProperty("insee_com") +"</h5></h5><h6> Statut : "+ event.feature.getProperty("statut") +"</h6> <h6> Population : "+ event.feature.getProperty("population") +"<hr /></h6>   <br><center><button onclick='getcomquart("+event.feature.getProperty("insee_com")+")' class='btn btnmr btn-info'>Réserver par quartiers</button><button onclick='reservcommune("+event.feature.getProperty("code")+")' class='btn btnmr btn-success'>Commune</button><button onclick='getCOMJSON("+event.feature.getProperty("code_dep")+")' class='btn btn-info'>Département</button></center></div>");
        infosdetailcommune(event.feature.getProperty("insee_com"));
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="initMap(\'departements.json\')" class="btn btnmr btn-warning" style="display: none"><i class="fa fa-arrow-circle-left" style="font-size:16px;" aria-hidden="true"></i>départements</button> <button onclick="getcomquart(' + event.feature.getProperty("insee_com") +')"class="btn btnmr margin-left btn-warning"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i> les Quartiers de la commune ' + event.feature.getProperty("nom_com") + ' </button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dep") + "\</h5> <h5 class='titlecont'>" + event.feature.getProperty("nom_com") + "</h5> <div id='infoscommune'><hr></div>  </div><div style='margin-bottom=10px;'><center><a href='#'  onclick='reservcommune(" + event.feature.getProperty("insee_com") + ")' class='btn btnmr btn-success btninfowindow' title='Réserver une commune '><i class='fa fa-shopping-cart'></i><i  style='font-style: normal;'> Réserver la commune(tous les quartiers)</i></a></center></div></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });

    map.data.forEach(function (feature) {
    });



    $("#alldepartement").click(function () {
        initMap()
    });
    activatesearche();
}



//Faire un Zoom sur une commune avec quartier*********************************************************************************************************************
function signlecomquartier(option, lat, lng) {
    var lat = parseFloat(lat);
    var lng = parseFloat(lng);
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12.5,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: lat, lng: lng,} //egion-Nouvelle-Aquitaine
    });
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });

    map.data.loadGeoJson(
        BASEURL + '' + option);

    $('#map').append("<div id='topmap'><button id='alldepartement' class=\"btn btn-warning\" style=\"background-color: #4cae4c; border-color: #4cae4c;margin: 20px;\"><i class=\"fa fa-arrow-circle-left\" style=\"font-size:16px;\" aria-hidden=\"true\"></i>Tous les départements</button></div><div class = 'input-field' id='searchdiv'> <input id='pac-input' class='form-control' placeholder='Adresse de mon business' type='text' size='50'><div class='icon-wrap' style='position: absolute;right: 4%; top:31px;'><svg class='bi bi-search' width='1em' height='1em' color='#ccc' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>\n" +
        "  <path fill-rule='evenodd' d='M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z' clip-rule='evenodd'/>\n" +
        "  <path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z' clip-rule='evenodd'/>\n" +
        "</svg></div></div>");
    btn_back_to_dep = "<button id = 'backdep' style = 'margin:20px;' onclick=\"getCOMJSON('"
    btn_back_to_dep = btn_back_to_dep.concat("')\" class='btn btn-warning'><i class='fa fa-arrow-circle-left' style='font-size:16px;' aria-hidden='true'></i> toutes les communes de ")
    btn_back_to_dep = btn_back_to_dep.concat("</button>")
    $('#topmap').append(btn_back_to_dep);



    map.data.setStyle(function (feature) {
        code_dept = feature.getProperty('code_dept');
        nom_dept = feature.getProperty('nom_dept');
        $("#backdep").attr('onclick', 'getCOMJSON('+code_dept+')')
        $("#backdep").html("<i class='fa fa-arrow-circle-left' style='font-size:16px;' aria-hidden='true'></i>toutes les communes de "+nom_dept)
        return ({
            fillColor: 'orange',
            strokeWeight: 1
        });
    });




    var data_layer = new google.maps.Data({map: map});


    //récuperer tous les fichier GeoJSON  pour les articles réserver
    $.ajax({
        url: BASEURL + 'Dbl/allfilequartierreserved/',
        type: 'GET', // type of the HTTP request
        success: function (response) {
            //  var string = JSON.stringify(response)
            var objects = JSON.parse(response)

            //Call function depcommunes pour afficher les communes
            // obj =  JSON.parse(response);
            //console.log(obj);

            objects.forEach(function (key, index) {


                data_layer.loadGeoJson(BASEURL + key.fichier);
                data_layer.setStyle({
                    fillColor: 'red',
                    strokeWeight: 1,
                    fillOpacity: 0.35
                });
            })

        }
    });

    map.data.addListener('click', function (event) {
        //show an infowindow on click
        //infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university"> DAFFEF' + event.feature.getProperty("nom_iris") + "\</h6> <h5>"+ event.feature.getProperty("nom_com") +"</h5></h5><h6>"+ event.feature.getProperty("nom_dept") +"</h6> <h6> <strong>Logements :</strong> "+ event.feature.getProperty("P15_LOG") +"</h6><smal><strong> Résidences :</strong> "+ event.feature.getProperty("P15_RP") +"</smal><smal> <strong>Maisons :</strong> "+ event.feature.getProperty("P15_MAISON") +"<hr /></smal>  <br><center><button onclick='getdepJSON("+event.feature.getProperty("insee_dep")+")' class='btn btnmr btn-info'>Quartiers</button><button onclick='reservquartier("+event.feature.getProperty("code_iris")+")' class='btn btnmr btn-success'>Réserver</button><button onclick='getsinglquartier("+event.feature.getProperty("code_iris")+")' class='btn btn-info'>Zoom</button></center></div>");
        infosdetailquartier(event.feature.getProperty("code_iris"))
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="getCOMJSON(' + event.feature.getProperty("code_dept") + ')" class="btn btnmr btn-warning"><i class="fa fa-arrow-circle-left" style="font-size:16px;" aria-hidden="true"></i> toutes les communes de ' + event.feature.getProperty("nom_dept") + '</button><button  onclick="initMap(\'departements.json\')" class="btn btnmr margin-left btn-warning" style="display: none"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i>Départements</button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dept") + "\</h5> <h5 class='titlecont'>" + event.feature.getProperty("nom_com") + "</h5><h5 class='titlecont'> Quartier : " + event.feature.getProperty("nom_iris") + "</h5><div id='infosquartier'><hr></div>  </div><div style='margin-bottom=10px;'><center><a href='#'  onclick='reservquartier(" + event.feature.getProperty("code_iris") + ")' class='btn btnmr btn-success btninfowindow' style='padding-top: 10px !important;' title='Réserver un quartier'><i class='fa fa-shopping-cart'></i><i style='font-style: normal;'> Réserver ce quartier</i></a><a href='#' style='background-color:dodgerblue;border-color:dodgerblue; padding-top: 10px !important;' onclick='reservcommune(" + event.feature.getProperty("insee_com") + ")' class='btn btnmr btn-success btninfowindow' title='Réserver la commune'><i class='fa fa-shopping-cart'></i><i style='font-style: normal'> Réserver la commune(tous les quartiers)</i></a></center></div></div>");


        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });

    data_layer.addListener('click', function (event) {
        //show an infowindow on click
        infosdetailquartier(event.feature.getProperty("code_iris"))
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="getCOMJSON(' + event.feature.getProperty("code_dept") + ')" class="btn btnmr btn-warning"><i class="fa fa-arrow-circle-left" style="font-size:16px;" aria-hidden="true"></i>toutes les communes de ' + event.feature.getProperty("nom_dept") + '</button><button  onclick="initMap(\'departements.json\')" class="btn btnmr margin-left btn-warning" style="display: none"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i>Départements</button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dept") + "\</h5> <h5 class='titlecont'>" + event.feature.getProperty("nom_com") + "</h5><h5 class='titlecont'> Quartier : " + event.feature.getProperty("nom_iris") + "</h5><div id='infosquartier'><hr></div>  </div><div style='margin-bottom=10px;'><center><h5 style='color: red'>Ce quartier est déjà réservé</h5><button onclick='removequartier(" + event.feature.getProperty("code_iris") + "," + event.feature.getProperty("insee_com") + ")' class='btn btnmr btn-warning'><i class='fa fa-trash' style='font-size:16px;' aria-hidden='true'></i>supprimer ce quartier</button></center></div></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);

    });

    marker.bindTo('map', data_layer);

    $("#alldepartement").click(function () {
        initMap()
    });

    activatesearche()
    /*----------------------------*/
}

//Faire un Zoom sur un quartier
function signlequartier(option, lat, lng) {
    var lat = parseFloat(lat);
    var lng = parseFloat(lng);
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        /* center: {lat: 46.394114 , lng:  -3.251577}, Pour la france */
        center: {lat: lat, lng: lng} //egion-Nouvelle-Aquitaine
    });
    map.data.loadGeoJson(
        BASEURL + '' + option);
    $('#map').append("<div id='topmap'><button id='alldepartement' class=\"btn btn-warning\" style=\"background-color: #4cae4c; border-color: #4cae4c;margin: 20px;\"><i class=\"fa fa-arrow-circle-left\" style=\"font-size:16px;\" aria-hidden=\"true\"></i>Tous les départements</button></div><div class = 'input-field' id='searchdiv'> <input id='pac-input' class='form-control' placeholder='Adresse de mon business' type='text' size='50'><div class='icon-wrap' style='position: absolute;right: 4%; top:31px;'><svg class='bi bi-search' width='1em' height='1em' color='#ccc' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>\n" +
        "  <path fill-rule='evenodd' d='M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z' clip-rule='evenodd'/>\n" +
        "  <path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z' clip-rule='evenodd'/>\n" +
        "</svg></div></div>");
    btn_back_to_dep = "<button id = 'backdep' style = 'margin:20px;' onclick=\"getCOMJSON('"
    btn_back_to_dep = btn_back_to_dep.concat("')\" class='btn btn-warning'><i class='fa fa-arrow-circle-left' style='font-size:16px;' aria-hidden='true'></i> toutes les communes de ")
    btn_back_to_dep = btn_back_to_dep.concat("</button>")
    $('#topmap').append(btn_back_to_dep);


    map.data.setStyle(function (feature) {
        code_dept = feature.getProperty('code_dept');
        nom_dept = feature.getProperty('nom_dept');
        $("#backdep").attr('onclick', 'getCOMJSON('+code_dept+')')
        $("#backdep").html("<i class='fa fa-arrow-circle-left' style='font-size:16px;' aria-hidden='true'></i>toutes les communes de "+nom_dept)
        return ({
            fillColor: 'orange',
            strokeWeight: 1
        });
    });

    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    map.data.addListener('click', function (event) {
        //show an infowindow on click
        //infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom_iris") + "\</h6> <h5>"+ event.feature.getProperty("nom_com") +"</h5></h5><h6>"+ event.feature.getProperty("nom_dept") +"</h6> <h6> <strong>Logements :</strong> "+ event.feature.getProperty("P15_LOG") +"</h6><smal><strong> Résidences :</strong> "+ event.feature.getProperty("P15_RP") +"</smal><smal> <strong>Maisons :</strong> "+ event.feature.getProperty("P15_MAISON") +"<hr /></smal>  <br><center><button onclick='getdepJSON("+event.feature.getProperty("insee_dep")+")' class='btn btnmr btn-info'>Quartiers</button><button onclick='reservquartier("+event.feature.getProperty("code_iris")+")' class='btn btnmr btn-success'>Réserver</button><button onclick='initMap(\"departements.json\")' class='btn btn-info'>Zoom</button></center></div>");
        infosdetailquartier(event.feature.getProperty("code_iris"))
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="getCOMJSON(' + event.feature.getProperty("code_dept") + ')" class="btn btnmr btn-warning"><i class="fa fa-arrow-circle-left" style="font-size:16px;" aria-hidden="true"></i> toutes les communes de ' + event.feature.getProperty("nom_dept") + '</button><button  onclick="initMap(\'departements.json\')" class="btn btnmr margin-left btn-warning" style="display: none"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i>Départements</button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dept") + "\</h5> <h5 class='titlecont'>" + event.feature.getProperty("nom_com") + "</h5><h5 class='titlecont'> Quartier : " + event.feature.getProperty("nom_iris") + "</h5><div id='infosquartier'><hr></div>  </div><div style='margin-bottom=10px;'><center><a href='#'  onclick='reservquartier(" + event.feature.getProperty("code_iris") + ")' class='btn btnmr btn-success btninfowindow' style='padding-top: 10px !important;' title='Réserver un quartier'><i class='fa fa-shopping-cart'></i><i style='font-style: normal;'> Réserver ce quartier</i></a><a href='#' style='background-color:dodgerblue;border-color:dodgerblue; padding-top: 10px !important;' onclick='reservcommune(" + event.feature.getProperty("insee_com") + ")' class='btn btnmr btn-success btninfowindow' title='Réserver la commune'><i class='fa fa-shopping-cart'></i><i style='font-style: normal'> Réserver la commune(tous les quartiers)</i></a></center></div></div>");

        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);
    });

    $("#alldepartement").click(function () {
        initMap()
    });
    activatesearche()

}

//Récupurer les coordonnée
function getPolygonCoords() {

    var len = bermudaTriangle.getPath().getLength();
    var htmlStr = "";
    for (var i = 0; i < len; i++) {
        htmlStr += bermudaTriangle.getPath().getAt(i).toUrlValue(6) + "<br>";
    }
    document.getElementById('info').innerHTML = htmlStr;

}

$(document).ready(function () {
    //Auto Load modal pour le choix des option
    if (configcmdjs == 2) {
        $('#ModalFT').modal({backdrop: 'static', keyboard: false}, 'show');
    }
    //les communces
    $('#search').keyup(function () {
        $('#result').html('');
        var searchField = $('#search').val();
        var expression = new RegExp('^' + searchField, "i");
        $.getJSON(BASEURL + 'repo/jsonall/communesfrance.json', function (data) {
            var count = 0;
            $.each(data, function (key, value) {
                if (value.nom_com.search(expression) != -1) {
                    $('#result').append('<li class="list-group-item"  onclick="getSNGLCOMJSON(\'' + value.insee_com + '\')"  >' + value.nom_com + '</li>');
                    count++;
                }
                if (count == 6) return false;
            });
        });
    });

    //les quartiers
    $('#searchqt').keyup(function () {
        $('#resultqt').html('');
        var searchField = $('#searchqt').val();
        var expression = new RegExp('^' + searchField, "i");
        $.getJSON(BASEURL + 'repo/jsonall/quartiersfrance.json', function (data) {
            var count = 0;
            $.each(data, function (key, value) {
                if (value.nom_iris.search(expression) != -1) {
                    $('#resultqt').append('<li class="list-group-item"  onclick="getsinglquartier(\'' + value.code_iris + '\')"  >' + value.nom_iris + '</li>');
                    count++;
                }
                if (count == 6) return false;
            });
        });
    });

});

//Gestion de la moteur de recherche  / Live Schearche
$(".search").show();
//Faire un Zoom sur une departement
/* les departements */
$("#searchdep").hide();
$("#resultdep").hide();
/* End  les departements */
/* les quartiers */
$("#searchqt").hide();
$("#resultqt").hide();

/* End  les quartiers */
function switchinput(option) {
    if (option == 2) {

        /* les communes */
        $("#search").show();
        $("#result").show();
        /* End  les communes */
        /* les departements */
        $("#searchdep").hide();
        $("#resultdep").hide();
        /* End  les departements */
        /* les quartiers */
        $("#searchqt").hide();
        $("#resultqt").hide();
        /* End  les quartiers */
    }
    if (option == 3) {
        /* les quartiers */
        $("#searchqt").show();
        $("#resultqt").show();
        /* End  les quartiers */


        /* les communes */
        $("#search").hide();
        $("#result").hide();
        /* End  les communes */
        /* les departements */
        $("#searchdep").hide();
        $("#resultdep").hide();
        /* End  les departements */
    }

}


//controle du bloc  choix  formule de distribution
function show1() {
    document.getElementById('choixformule').style.display = 'none';//Block choix formule
    document.getElementById('validation').style.display = 'block';//Bouton validation

    document.getElementById("formule").selectedIndex = 0;


}

function show2() {
    document.getElementById('choixformule').style.display = 'block';//Block choix formule
    document.getElementById('validation').style.display = 'none'; //Bouton validation
    document.getElementById("formule").selectedIndex = 0;
}


//Supprimer un quartier un quartier
function removequartier(code_iris, insee_com) {

    $.ajax({
        url: BASEURL + 'Appdev/remove_quartier/' + code_iris + '/' + insee_com,
        type: 'GET', // type of the HTTP request
        success: function (data) {

            quartierslist();
            pricelivraison();
            totalstartcmd();
            communeslistsajax();
            getcomquart(insee_com);
            notificationtostremovequartier();
        }
    });

}

//Supprimer une communie
function removecommune(insee_com) {

    $.ajax({
        url: BASEURL + 'Appdev/remove_commune/' + insee_com,
        type: 'GET', // type of the HTTP request
        success: function (data) {
            quartierslist();
            pricelivraison();
            totalstartcmd();
            communeslistsajax();
            getcomquart(insee_com);
            allquartierreserv();
            notificationtostremovecommune();
        }
    });
}

//Liste des quartier de la commande
function quartierslist() {
    $.ajax({
        url: BASEURL + 'Appdev/quartierslist/',
        type: 'GET', // type of the HTTP request
        success: function (data) {
            $('#quartier-reserver').html(data);

        }
    });
}

//Liste des commune après  l'ajax
function communeslistsajax() {
    $.ajax({
        url: BASEURL + 'Appdev/commune_cmd/',
        type: 'GET', // type of the HTTP request
        success: function (data) {
            $('#communeslist').html(data);


        }
    });
}

//Liste des commune après  l'ajax
function totalstartcmd() {
    $.ajax({
        url: BASEURL + 'Appdev/totalcmd_dbl/',
        type: 'GET', // type of the HTTP request
        success: function (data) {
            $('#summercmd').html(data);
        }
    });
}

//Calcule livraison
function pricelivraison() {
    $.ajax({
        url: BASEURL + 'Dbl/methodelivraison/',
        type: 'GET', // type of the HTTP request
        success: function (data) {

            $('#methodelivraison').html(data);


        }
    });
}

//GET INFO COMMUNE
function infosdetailcommune(insee_com) {

    $.ajax({
        url: BASEURL + 'Tools/infoscom/' + insee_com,
        type: 'GET', // type of the HTTP request
        success: function (data) {

            $('#infoscommune').html(data);


        }
    });
}

//GET INFO COMMUNE
function infosdetailquartier(code_iris) {

    $.ajax({
        url: BASEURL + 'Tools/infoquartier/' + code_iris,
        type: 'GET', // type of the HTTP request
        success: function (data) {

            $('#infosquartier').html(data);


        }
    });
}

//GET MAP ALL QUARTIER
//Avoir les méthode de livrais
function allquartierreserv() {
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    var data_layer = new google.maps.Data({map: map});
    //récuperer tous les fichier GeoJSON  pour les articles réserver
    $.ajax({

        url: BASEURL + 'Dbl/allfilequartierreserved/',
        type: 'GET', // type of the HTTP request
        success: function (response) {


            //  var string = JSON.stringify(response)
            var objects = JSON.parse(response)

            //Call function depcommunes pour afficher les communes
            // obj =  JSON.parse(response);

            console.log(response);

            objects.forEach(function (key, index) {

                data_layer.loadGeoJson(BASEURL + key.fichier);
                data_layer.setStyle({
                    fillColor: 'red',
                    strokeWeight: 1,
                    fillOpacity: 0.35
                });
            })
            $("#map").show();

        }
    });
    data_layer.addListener('click', function (event) {
        //show an infowindow on click
        swal("Oh non!", "Cette zone est réservé", "error");

    });
}

//Single commune
function allquartierreservsgncom(insee_com) {
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    var data_layer = new google.maps.Data({map: map});
    //récuperer tous les fichier GeoJSON  pour les articles réserver
    $.ajax({

        url: BASEURL + 'Dbl/allquartierforcommune/' + insee_com,
        type: 'GET', // type of the HTTP request
        success: function (response) {


            //  var string = JSON.stringify(response)
            var objects = JSON.parse(response)

            //Call function depcommunes pour afficher les communes
            // obj =  JSON.parse(response);

            console.log(response);

            objects.forEach(function (key, index) {

                data_layer.loadGeoJson(BASEURL + key.fichier);
                data_layer.setStyle({
                    fillColor: 'red',
                    strokeWeight: 1,
                    fillOpacity: 0.35
                });
            })
            $("#map").show();

        }
    });
    data_layer.addListener('click', function (event) {
        //show an infowindow on click
        infosdetailcommune(event.feature.getProperty("insee_com"));
        var status = event.feature.getProperty("statut");
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="initMap(\'departements.json\')" class="btn btnmr btn-warning" style="display: none"><i class="fa fa-arrow-circle-left" style="font-size:16px;" aria-hidden="true"></i>départements</button>  <button onclick="getcomquart(' + event.feature.getProperty("insee_com") + ')"class="btn btnmr margin-left btn-warning"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i>les Quartiers de la commune ' + event.feature.getProperty("nom_com") + '</button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dept") + "\</h5> <h5 class='titlecont'>" + event.feature.getProperty("nom_com") + "</h5> <div id='infoscommune'><hr></div> <center><div><h5 style='color: red'> Cette commune est déjà réservée</h5><button onclick='removecommune(" + event.feature.getProperty("insee_com") + ")' class='btn btnmr btn-warning'><i class='fa fa-trash' style='font-size:16px;' aria-hidden='true'></i>supprimer cette commune</button></div></center></div></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);

    });
}

//Signble article
function snglquartierreserv(code_iris) {
    infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    var data_layer = new google.maps.Data({map: map});
    //récuperer tous les fichier GeoJSON  pour les articles réserver
    $.ajax({

        url: BASEURL + 'Dbl/signlequartierreserved/' + code_iris,
        type: 'GET', // type of the HTTP request
        success: function (response) {

            //  var string = JSON.stringify(response)
            var objects = JSON.parse(response)

            //Call function depcommunes pour afficher les communes
            // obj =  JSON.parse(response);

            console.log(response);

            objects.forEach(function (key, index) {

                data_layer.loadGeoJson(BASEURL + key.fichier);
                data_layer.setStyle({
                    fillColor: 'red',
                    strokeWeight: 1,
                    fillOpacity: 0.35
                });
            })
            $("#map").show();

        }
    });
    data_layer.addListener('click', function (event) {
        //show an infowindow on click
        infosdetailquartier(event.feature.getProperty("code_iris"))
        infoWindow.setContent('<div style="overflow:hidden;white-space:nowrap; "><button onclick="getCOMJSON(' + event.feature.getProperty("code_dept") + ')" class="btn btnmr btn-warning"><i class="fa fa-arrow-circle-left" style="font-size:16px;" aria-hidden="true"></i> toutes les communes de ' + event.feature.getProperty("nom_dept") + '</button><button  onclick="initMap(\'departements.json\')" class="btn btnmr margin-left btn-warning" style="display: none"><i class="fas fa-eye  " style="font-size:16px; margin-right:10px;"></i>Départements</button><hr> <div style="height:auto;"><h5 class="titlecont">' + event.feature.getProperty("nom_dept") + "\</h5> <h5 class='titlecont'>" + event.feature.getProperty("nom_com") + "</h5><h5 class='titlecont'> Quartier : " + event.feature.getProperty("nom_iris") + "</h5><div id='infosquartier'><hr></div>  </div><div style='margin-bottom=10px;'><center><h5 style='color: red'>Ce quartier est déja réservé</h5><button onclick='removequartier(" + event.feature.getProperty("code_iris") + "," + event.feature.getProperty("insee_com") + ")' class='btn btnmr btn-warning'><i class='fa fa-trash' style='font-size:16px;' aria-hidden='true'></i>supprimer ce quartier</button></center></div></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position", event.latLng);
        infoWindow.open(map, anchor);

    });
}

//Toast notification
function notificationtost() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

function notificationtostremovequartier() {
    var x = document.getElementById("remq");
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

//TOAST ADD COMMUNER
function notificationtostcommune() {
    var x = document.getElementById("snackbarcom");
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

//TOAST REMOVE COmmune

function notificationtostremovecommune() {
    var x = document.getElementById("remqcom");
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}


function loading(count) {
    var num = 0;

    for (i = 0; i <= count; i++) {
        setTimeout(function () {
            $('#loadernum').html(num + ' quartiers sur ' + count);
            num++;
        }, i * 120);
    }

}


//Calcule total de la commande
//End contrôle de  bloc