$(document).ready(function () {

    // Step show event
    $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {
        //alert("You are on step "+stepNumber+" now");
        if (stepPosition === 'first') {
            $("#prev-btn").addClass('disabled');
        }
        if (stepPosition === 'final') {
            $(".sw-btn-group-extra").css("width", "100%");
            $(".confircommande").css("float", "right");
            $(".annulercomande").css("float", "left");

        } else {
            $(".sw-btn-group-extra").css("width", "");
        }
        /* else if (stepPosition === 'final') {
            $("#next-btn").addClass('disabled');
        } else {
            $("#prev-btn").removeClass('disabled');
            $("#next-btn").removeClass('disabled');
        } */
    });

    // Toolbar extra buttons
    var btnFinish = $('<button></button>').text('Confirmer la commande')
        .addClass('btn btn-info btn-final confircommande')
        .attr("id", "btnfinal")

    var btnCancel = $('<button></button>').text(' Annuler ma commande')
        .addClass('btn btn-danger annulercomande')
        .on('click', function () {
            $('#smartwizard').smartWizard("reset");
        });

//                                             .css("display", "none")
    // .on('click', function () {
    //    checkBox = document.getElementById("terms");
    ///    if (checkBox.checked == false) {
    //        alert('Vous devez accepter les conditions générale de vente ');
    //   }
    //  });

    // Smart Wizard
    $('#smartwizard').smartWizard({
        selected: 0,
        theme: 'default',
        transitionEffect: 'fade',
        showStepURLhash: true,
        toolbarSettings: {
            toolbarPosition: 'bottom',
            toolbarButtonPosition: 'end',
            toolbarExtraButtons: [btnFinish, btnCancel]
        }
    });
    // $(".annulercomande:eq(0)").css("display","none");


    // External Button Events
    $("#reset-btn").on("click", function () {
        // Reset wizard
        $('#smartwizard').smartWizard("reset");
        $('.btn-final').hide();
        return true;
    });

    $("#prev-btn").on("click", function () {
        // Navigate previous
        $('#smartwizard').smartWizard("prev");
        return true;
    });

    $("#next-btn").on("click", function () {
        // Navigate next
        $('#smartwizard').smartWizard("next");
        return true;
    });

    $("#theme_selector").on("change", function () {
        // Change theme
        $('#smartwizard').smartWizard("theme", $(this).val());
        return true;
    });

    // Set selected theme on page refresh
    $("#theme_selector").change();


    //création/update de la commande en cas de passage d'une étape à une autres

    $(".confirmstepcmd").click(function (e) {
        e.preventDefault();
        var message = $('textarea[name=message]').val(); // message
        var cgv = $('input[name=cgv]:checked').val(); //Condition géneral de vente
        if (cgv) {
            cgv = 1;
        } else {
            cgv = 0;
        }
        var prixht = $("#prixht").val();
        var pourcentva = $("#pourcentva").val();
        var tva = $("#tva").val();
        var prixttc = $("#prixttc").val();
        jQuery.ajax({
            url: BASEURL + "Appdev/save_cmd/",
            type: 'GET',
            data: "message=" + message + "&prixht=" + prixht
                + "&pourcentva=" + pourcentva + "&tva=" + tva + "&prixttc=" + prixttc + "&cgv=" + cgv,
            success: function (response) {
                console.log(response)

            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal({
                    title: "Une erreur s'est produit lors de l'enregistrement de la commande. Veuillez refaire",
                    icon: "error",
                });
            }
        });
    });


//annulation de la commande
    $(".annulercomande").click(function (e) {
        swal({
            title: "Êtes-vous sûre?",
            text: "Vous voulez annuler cette commande!",
            icon: "warning",
            closeOnClickOutside: false,
            closeOnEsc: false,
            buttons: ["Annuler", "Valider"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    //traitement ajax
                    $.ajax({
                        url: BASEURL + "Dbl/annulercmd/",
                        type: "Get", // Le type de la requête HTTP,
                        dataType: 'html',
                        success: function (response) {


                            swal({
                                title: "Annulation efféctué avec succé",
                                icon: "success",
                            });
                            window.location.replace(BASEURL + "App");

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal({
                                title: "Erreur d'annulation",
                                icon: "error",
                            });
                        }
                    });
                } else {
                    swal({
                        title: "Vous avez choisi de ne pas annuler cette commande",
                        icon: "info",
                    });
                }
            });
    });

    $(".confircommande").click(function (e) {
        var message = $('textarea[name=message]').val(); // message
        jQuery.ajax({
            url: BASEURL + "Appdev/countquartiersreserved/",
            type: 'GET',
            success: function (response) {
                obj = JSON.parse(response);
                if (response == 0) {
                    swal({
                        title: "Il y a aucun quartier réservé",
                        icon: "error",
                    });
                } else {
                    swal({
                        title: "Êtes-vous sûre?",
                        text: "Vous voulez confirmer cette commande",
                        icon: "warning",
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                        buttons: ["Annuler", "Valider"],
                        dangerMode: true,
                    })
                        .then((willDelete) => {
                            if (willDelete) {
                                //redirection vers la partie traitement de la commande
                                if (account_ok) {
                                    jQuery.ajax({
                                        url: BASEURL + "Appdev/confirmcmd/",
                                        type: 'GET',
                                        data: "message=" + message,
                                        dataType: 'json',
                                        success: function (response) {
                                            if (response.sucess) {
                                                swal({
                                                    title: "Commande confirmée avec succé",
                                                    icon: "success",
                                                });
                                                window.location.replace(BASEURL + "Appdev/paiement");
                                            } else {
                                                swal("Attention!", response.msgerror, "error");

                                            }

                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            swal({
                                                title: "Une erreur s'est produit lors de la confirmation de la commande, Veuillez refaire",
                                                icon: "error",
                                            });
                                        }
                                    });
                                } else {

                                    swal({
                                        title: "Attention !",
                                        text: "Vous n'avez pas remplis toutes les informations nécessaires de votre compte.",
                                        icon: "warning",
                                        closeOnClickOutside: false,
                                        closeOnEsc: false,
                                        buttons: ["Annuler", "Voir mon compte"],
                                        dangerMode: true,
                                    })
                                        .then((willDelete) => {
                                            if (willDelete) {
                                                window.location.replace(BASEURL + "administrator/distri_comptes/view/0");
                                            }
                                        });
                                }
                            } else {
                                swal({
                                    title: "Vous avez choisi de ne pas confirmer la commande courante",
                                    icon: "info",
                                });
                            }
                        });
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal({
                    title: "Une erreur s'est produit lors de la confirmation de la commande, Veuillez refaire",
                    icon: "error",
                });
            }
        });


    });


////chargement du message ds la partie recap

    $("#message").blur(function () {
        var msg = $("#message").val();
        document.getElementById("messageClient").innerHTML = msg;
    });
});


///attribution et modification des moyens de payement


function validermp(idmp) {

    swal({
        title: "Êtes-vous sûre?",
        text: "Vous voulez choisir cette méthode de payement!",
        icon: "warning",
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons: ["Annuler", "Valider"],
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                //traitement ajax
                $.ajax({
                    url: BASEURL + "Appdev/save_myp/",
                    type: "Get", // Le type de la requête HTTP
                    data: "mp=" + idmp,
                    dataType: 'html',
                    success: function (response) {


                        swal({
                            title: "Enregistrement efféctué avec succé",
                            icon: "success",
                        });
                        $('#iconPatCheq').removeClass('fa-info-circle').addClass('fa-check-circle');
                        window.location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal({
                            title: "Erreur d'enregistrement",
                            icon: "error",
                        });
                    }
                });
            } else {
                swal({
                    title: "Vous avez choisi de ne pas enregistrer votre moyen de payement",
                    icon: "info",
                });
            }
        });


}

function modifmp() {
    $(".affichedetailmp").hide();
    $(".choixmp").show();
}

////    js accordio
//changer icone en clic sur boutton
$("#btPayChec").click(function () {
    if ($("#btPayChec").hasClass("collapsed")) {
        $('#iconPatCheq').removeClass('fa-arrow-circle-down').addClass('fa-info-circle');
    } else {
        $('#iconPatCheq').removeClass('fa-info-circle').addClass('fa-arrow-circle-down');
    }
});

$("#btPayBanq").click(function () {
    val = $("#iconPatBanq").attr('class');
    if ($("#btPayBanq").hasClass("collapsed")) {
        $('#iconPatBanq').removeClass('fa-check-circle').addClass('fa-info-circle');
    } else {
        $('#iconPatBanq').removeClass('fa-info-circle').addClass('fa-check-circle');
    }
});

$("#btPayPaypal").click(function () {
    val = $("#iconPatPaypal").attr('class');
    if ($("#btPayPaypal").hasClass("collapsed")) {
        $('#iconPatPaypal').removeClass('fa-check-circle').addClass('fa-info-circle');
    } else {
        $('#iconPatPaypal').removeClass('fa-info-circle').addClass('fa-check-circle');
    }
});

$("#btPayVir").click(function () {
    console.log($("#btPayVir").hasClass("collapsed"));
    if ($("#btPayVir").hasClass("collapsed")) {
        $('#iconPatVir').removeClass('fa-check-circle').addClass('fa-info-circle');
    } else {
        $('#iconPatVir').removeClass('fa-info-circle').addClass('fa-check-circle');
    }
});



