var map;
//Chargement de la map
function initMap() {
        //var option ='cartefrance.json';
         var option ='montpellier.geojson';
         var BASEURL = 'https://apps.distripub.fr/';
        //Declaration de la map
      
         map = new google.maps.Map(document.getElementById('map'), {
          zoom: 5.8,
         center: {lat:  47, lng:  3.34,} //egion-Nouvelle-Aquitaine
        }); 
         infoWindow = new google.maps.InfoWindow({
          content: ""
        });
         var data_layer = new google.maps.Data({map: map});


        //récuperer tous les fichier GeoJSON  pour les articles réserver
        $.ajax({
        url : BASEURL+'Dbl/allfilequartierreserved/', 
        type : 'GET', // type of the HTTP request
        success : function(response)
        {
        //  var string = JSON.stringify(response)
         var objects = JSON.parse(response)

                
           objects.forEach(function(key, index) {

        
          data_layer.loadGeoJson(BASEURL+key.fichier);
        data_layer.setStyle({
            fillColor: 'red',
          strokeWeight: 1,
           fillOpacity: 0.35
        });
        })  
      
        }
        });
         data_layer.addListener('click', function(event)
        {
        //show an infowindow on click   
        swal("Oh non!", "Cette zone est réservé", "error");
    
       });

      map.data.loadGeoJson(BASEURL+'kml/'+option);



      map.data.setStyle({
          fillColor: 'green',
          strokeWeight: 1
        });
     
                 
     infoWindow = new google.maps.InfoWindow({
		      content: ""
		  	});
        map.data.addListener('click', function(event)
        {
        //show an infowindow on click   
   infoWindow.setContent('<div style="line-height:0;overflow:hidden;white-space:nowrap;"><h6 id="university">' + event.feature.getProperty("nom") + "\</h6>   <br><center><button onclick='getCOMJSON("+event.feature.getProperty("code")+")' class='btn btn-primary'>Les communes</button></center></div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position",event.latLng);
        infoWindow.open(map,anchor);
      });
        
      
        
}