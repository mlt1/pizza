/*replace all string*/

$(document).ready(function () {

    // Step show event
    $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {
        //alert("You are on step "+stepNumber+" now");
        if (stepPosition === 'first') {
            $("#prev-btn").addClass('disabled');
        } else if (stepPosition === 'final') {
            $("#next-btn").addClass('disabled');
        } else {
            $("#prev-btn").removeClass('disabled');
            $("#next-btn").removeClass('disabled');
        }
    });

    // Toolbar extra buttons
    var btnFinish = $('<button></button>').text('Confirmer la commande')
            .addClass('btn btn-info btn-final')
            .attr("id", "btnfinal" )
            .attr("class", "confircommande btn-info" )
//                                             .css("display", "none")
//            .on('click', function () {
//                checkBox = document.getElementById("terms");
//                if (checkBox.checked == false) {
//                    alert('Vous devez accepter les conditions générale de vente ');
//                }
//            });

    var btnCancel = $('<button></button>').text('Annuler')
            .addClass('btn btn-danger annulercomande')
//    .css("display", "none")
            .on('click', function () {
                $('#smartwizard').smartWizard("reset");
            });


    // Smart Wizard
    $('#smartwizard').smartWizard({
        selected: 0,
        theme: 'default',
        transitionEffect: 'fade',
        showStepURLhash: true,
        toolbarSettings: {toolbarPosition: 'both',
            toolbarButtonPosition: 'end',
            toolbarExtraButtons: [btnFinish, btnCancel]
        }
    });


    // External Button Events
    $("#reset-btn").on("click", function () {
        // Reset wizard
        $('#smartwizard').smartWizard("reset");
        $('.btn-final').hide();
        return true;
    });

    $("#prev-btn").on("click", function () {
        // Navigate previous
        $('#smartwizard').smartWizard("prev");
        return true;
    });

    $("#next-btn").on("click", function () {
        // Navigate next
        $('#smartwizard').smartWizard("next");
        return true;
    });

    $("#theme_selector").on("change", function () {
        // Change theme
        $('#smartwizard').smartWizard("theme", $(this).val());
        return true;
    });

    // Set selected theme on page refresh
    $("#theme_selector").change();
});


////    js accordio
//changer icone en clic sur boutton
$("#btPayChec").click(function () {
    if ($("#btPayChec").hasClass("collapsed")) {
        $('#iconPatCheq').removeClass('fa-check-circle').addClass('fa-info-circle');
    } else {
        $('#iconPatCheq').removeClass('fa-info-circle').addClass('fa-check-circle');
    }
});

$("#btPayBanq").click(function () {
    val = $("#iconPatBanq").attr('class');
    if ($("#btPayBanq").hasClass("collapsed"))
    {
        $('#iconPatBanq').removeClass('fa-check-circle').addClass('fa-info-circle');
    } else {
        $('#iconPatBanq').removeClass('fa-info-circle').addClass('fa-check-circle');
    }
});

$("#btPayPaypal").click(function () {
    val = $("#iconPatPaypal").attr('class');
    if ($("#btPayPaypal").hasClass("collapsed")) {
        $('#iconPatPaypal').removeClass('fa-check-circle').addClass('fa-info-circle');
    } else {
        $('#iconPatPaypal').removeClass('fa-info-circle').addClass('fa-check-circle');
    }
});
$('.collapse').collapse;
$("#btPayVir").click(function () {
    console.log($("#btPayVir").hasClass("collapsed"));
    if ($("#btPayVir").hasClass("collapsed")) {
        $('#iconPatVir').removeClass('fa-check-circle').addClass('fa-info-circle');
    } else {
        $('#iconPatVir').removeClass('fa-info-circle').addClass('fa-check-circle');
    }
});
$("#ajoutmsgcmd").click(function(){alert($("#formMSG").val());
    $.ajax({
       url : 'send_mail.php',
       type : 'POST', // Le type de la requête HTTP, ici devenu POST
       data : 'email=' + email + '&contenu=' + contenu_mail, // On fait passer nos variables, exactement comme en GET, au script more_com.php
       dataType : 'html'
    });
   
});
