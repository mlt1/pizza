
/***********************************************************************************/
/* Mise à jours  status  de la commande  par l'administrateur
/***********************************************************************************/

$(document).ready(function(){
	   $("#validation").click(function (e) {
	            e.preventDefault(); 
	            var configformulecmd = $('input[name=regcmd]:checked').val();
	            var formulecmd = $('select[name=formule]').val();
	              $.ajax({
	            url : BASEURL+'Dbl/configstartcmd/'+configformulecmd+'/'+formulecmd, 
	            type : 'GET', // type of the HTTP request
	            success : function(data)
	            {
	            
	                swal("Choix a été enregistré", "Configuration a été effectuée avec succès!", "success");  
	          
	           $(".swal-button").click(function (e) {
	               
	               $('#ModalFT').modal('hide');
	               location.reload();
	            });
	            }
		});
});  