<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Récuperation formats documents
function formatdocument(){
     $CI = & get_instance();
     $where = "statusformat= 1"; // on récupère seullement les format active
     $CI->db->select('*');
     $CI->db->from('ipw_formatdoc');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation orientationpage
function orientationpage(){
     $CI = & get_instance();
     $where = "statusorientationpage= 1"; // on récupère seullement les orientations active     
     $CI->db->select('*');
     $CI->db->from('ipw_orientationpage');
     $CI->db->where($where);     
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation propriétépage
function proporietepage(){
     $CI = & get_instance();
     $CI->db->select('*');
     $where = "statusproprietepage= 1"; // on récupère seullement les propriétés active          
     $CI->db->from('ipw_proprietepage');
     $CI->db->where($where);          
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation les options viseul
function optionsviseul(){
     $CI = & get_instance();
     $where = "statusvisueldocument= 1"; // on récupère seullement les format active
     $CI->db->select('*');
     $CI->db->from('ipw_visueldocument');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation liste des grammage
function getgrammages(){
     $CI = & get_instance();
     $where = "statusdocgrammage= 1"; // on récupère seullement les format active
     $CI->db->select('*');
     $CI->db->from('ipw_docgrammage');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation la liste formules de distribution
function getformulesdistribution(){
     $CI = & get_instance();
     $where = "statusformuledistribution= 1"; // on récupère seullement les format active
     $CI->db->select('*');
     $CI->db->from('ipw_formuledistribution');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation moyen de payement
function getmp(){
     $CI = & get_instance();
     $where = "statusmp= 1"; // on récupère seullement les moyens active
     $CI->db->select('*');
     $CI->db->from('ipw_moyen_payement');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//récuperation le reglation de la commande
function getregcmd(){
     $CI = & get_instance();
     $where = "statusregcmd= 1"; // on récupère seullement les moyens active
     $CI->db->select('*');
     $CI->db->from('ipw_cmdreglage');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation Formule choisie et aussi le aussi type de configuration de la commande
function getconfcmd($idscmd =''){
     $CI = & get_instance();   
     $CI->db->select('*');
     $CI->db->from('ipw_startcmd');
     $CI->db->where('idscmd',$idscmd);
     $query = $CI->db->get();
    $records = $query->row();
return $records;
}

//Récuperation du condition générale de vente pour le service print
function getcgv($idscmd =''){
     $CI = & get_instance(); 
     $where = "statuscgv= 1"; // on récupère seullement les cgv active     
     $CI->db->select('*');
     $CI->db->from('ipw_cgv');
     $CI->db->where('cdcatservice',$idscmd);
     $query = $CI->db->get();
    $records = $query->row();
return $records;
}
//Récupération devise
function getdevise() {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_devise a
    WHERE a.statusdevise = 1 "; // on récupère seullement les devises active
    $query = $CI->db->query($sql);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}
//Récupération du moyen de payement
function mp() {
     $CI = & get_instance();
     $where = "statusmp= 1"; // on récupère seullement les mp active
     $CI->db->select('*');
     $CI->db->from('ipw_moyen_payement');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récupération du détail de la commande
function detailcmd() {
    $key=$_SESSION["cmkey"];
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_commande a
    WHERE a.keyuniq ='".$key."'"; // on récupère seullement les informations  actives
    $query = $CI->db->query($sql);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}
//Récupération du détail de la commande du table Ipw_cmdprint
function detailcmdprint($idcmd) {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM Ipw_cmdprint a
    WHERE a.idcmd =".$idcmd; // on récupère seullement les informations  actives
    $query = $CI->db->query($sql,$idcmd);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}


//Récupération dle moyen de pyement adopté
function mpchoisie($idmp) {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_moyen_payement a
    WHERE a.idmp='".$idmp."'"; // on récupère seullement les informations  actives
    $query = $CI->db->query($sql,$idmp);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

