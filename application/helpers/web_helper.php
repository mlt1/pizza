<?php

function getregcmd(){
     $CI = & get_instance();
     $where = "statusregcmd= 1"; // on récupère seullement les moyens active
     $CI->db->select('*');
     $CI->db->from('ipw_cmdreglage');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récuperation Formule choisie et aussi le aussi type de configuration de la commande
function getconfcmd($idscmd =''){
     $CI = & get_instance();   
     $CI->db->select('*');
     $CI->db->from('ipw_startcmd');
     $CI->db->where('idscmd',$idscmd);
     $query = $CI->db->get();
    $records = $query->row();
return $records;
}
//Récupération du détail de la commande
function detailcmd() {
    $key=$_SESSION["cmdwebkey"];
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_commande a
    WHERE a.keyuniq ='".$key."'"; // on récupère seullement les informations  actives
    $query = $CI->db->query($sql);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}
//Récuperation du condition générale de vente pour le service print
function getcgv($idscmd =''){
     $CI = & get_instance(); 
     $where = "statuscgv= 1"; // on récupère seullement les cgv active     
     $CI->db->select('*');
     $CI->db->from('ipw_cgv');
     $CI->db->where('cdcatservice',$idscmd);
     $query = $CI->db->get();
    $records = $query->row();
return $records;
}
//Récupération du moyen de payement
function mp() {
     $CI = & get_instance();
     $where = "statusmp= 1"; // on récupère seullement les mp active
     $CI->db->select('*');
     $CI->db->from('ipw_moyen_payement');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
//Récupération dle moyen de pyement adopté
function mpchoisie($idmp) {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_moyen_payement a
    WHERE a.idmp='".$idmp."'"; // on récupère seullement les informations  actives
    $query = $CI->db->query($sql,$idmp);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}