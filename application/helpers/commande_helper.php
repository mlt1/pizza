<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Récupération du nom de société
function doneesociete() {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_societe a
    WHERE a.statussociete = 1 "; // on récupère seullement les informations  actives
    $query = $CI->db->query($sql);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récupération devise
function getdevise() {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_devise a
    WHERE a.statusdevise = 1 "; // on récupère seullement les devises active
    $query = $CI->db->query($sql);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récupération information clients
function getinfoclient($iduser = '') {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM distri_comptes a
    WHERE a.iduser = " . $iduser . " ";
    $query = $CI->db->query($sql, $iduser);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récupération type de la commande
function gettypecmd($codetype = '') {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_typecmd a
    WHERE 	a.codetypecmd = " . $codetype . " ";
    $query = $CI->db->query($sql, $codetype);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récuperation préfixe numérotation
function prefixnumerotation() {
    $CI = & get_instance();
    $CI->db->select('*');
    $CI->db->from('ipw_numbring');
    $query = $CI->db->get();
    $records = $query->result_array();
    return $records;
}

//Récupération les quartiers de la commande  boite au lettre
function getcartiercmd($key = '') {
    $CI = & get_instance();
    $where = "	cmdkey= '" . $key . "' AND `statuscmq` =1";
    $CI->db->select('*');
    $CI->db->from('ipw_quartiercmd');
    $CI->db->where($where);
    $query = $CI->db->get();
    $records = $query->result_array();
    return $records;
}

//Récupération les cartiers de la commande  boite au lettre
function donneequartier($idq) {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_quartier_map a
    WHERE 	a.idq = " . $idq . " and a.status=1";
    $query = $CI->db->query($sql, $idq);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récupération le groupe de l'utilisateur connecté
function groupeuser($idu) {
    $CI = & get_instance();
    $sql = "SELECT `group_id`
    FROM aauth_user_to_group a
    WHERE 	a.user_id = " . $idu . "";
    $query = $CI->db->query($sql, $idu);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récupération l'option visuel adopté par le client
function getvisuelcmd($keycmd) {
    $CI = & get_instance();
    $sql = "SELECT `visuel`
    FROM ipw_startcmd a
    WHERE a.cmdkey = '" . $keycmd . "' and `statuscmd` =1";
    $query = $CI->db->query($sql, $keycmd);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récupération les détail de l'option visuel choisie
function getdetailvisuel($idv) {
    $CI = & get_instance();
    $sql = "SELECT *
    FROM ipw_visueldocument a
    WHERE 	a.codevisueldocument = " . $idv . " and `statusvisueldocument` = 1";
    $query = $CI->db->query($sql, $idv);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récupération les options de la commandes
function getoptioncmd($keycmd) {
    $CI = & get_instance();
    $sql = "SELECT *
    FROM ipw_startcmd a
    WHERE a.cmdkey = '" . $keycmd . "' and a.`statuscmd` =1 ";
    $query = $CI->db->query($sql, $keycmd);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récuperation formats documents
function formatdocument($formdoc) {
    $CI = & get_instance();
    $sql = "SELECT *
    FROM ipw_formatdoc a
    WHERE a.codeformat = " . $formdoc . " and a.`statusformat` =1 "; // on le format en cas ou il est actif
    $query = $CI->db->query($sql, $formdoc);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récuperation liste des grammage
function getgrammages($idg) {
    $CI = & get_instance();
    $sql = "SELECT *
    FROM ipw_docgrammage a
    WHERE a.statusdocgrammage= 1 and a.`codedocgrammage`=" . $idg; // on a le grammage en cas ou il est actif
    $query = $CI->db->query($sql, $idg);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récuperation la liste formules de distribution
function getformulesdistribution($idf) {
    $CI = & get_instance();
    $sql = "SELECT *
    FROM ipw_formuledistribution a
    WHERE a.statusformuledistribution= 1 and a.`codeformuledistribution`=" . $idf; // on a le formule en cas ou il est actif
    $query = $CI->db->query($sql, $idf);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récuperation palier grammage
function getpaliergramm($idp) {
    $CI = & get_instance();
    $sql = "SELECT *
    FROM  ipw_paliergrammage a
    WHERE a.`statuspaliergrammage`= 1 and a.`codepaliergrammage`=" . $idp; // on a le formule en cas ou il est actif
    $query = $CI->db->query($sql, $idp);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récuperation élément table status
function getstatus() {
    $CI = & get_instance();

    $CI->db->select('*');
    $CI->db->from('distri_statuts');
    $query = $CI->db->get();
    $records = $query->result_array();
    return $records;
}

//Récuperation palier grammage
function havemission($idcmd, $titre) {
    $CI = & get_instance();
    $sql = "SELECT *
    FROM  distri_missions 
    WHERE `idorder`= " . $idcmd ; // on a le formule en cas ou il est actif
    $query = $CI->db->query($sql, $idcmd, $titre);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récuperation des divers mission de la commande
function getmissioncmd($numcmd) {
    $CI = & get_instance();

    $CI->db->select('*');
    $where = "idorder= " . $numcmd; // on récupère seullement les format active
    $CI->db->from('distri_missions');
    $CI->db->where($where);
    $query = $CI->db->get();
    $records = $query->result_array();
    return $records;
}

//Récuperation nom du  status
function getsnametatus($codestatus) {
    $CI = & get_instance();
    $sql = "SELECT *
    FROM  distri_statuts a
    WHERE a.`codestatus`= " . $codestatus; // on a le formule en cas ou il est actif
    $query = $CI->db->query($sql, $codestatus);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récupération dle moyen de pyement adopté
function mpchoisie($idmp) {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_moyen_payement a
    WHERE a.idmp='".$idmp."'"; // on récupère seullement les informations  actives
    $query = $CI->db->query($sql,$idmp);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

//Récupération détail de la commande print  
function recupdetailprint($idcmdp) {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM Ipw_cmdprint a
    WHERE a.idcmd=".$idcmdp; // on récupère une ligne bien définie de la commande print
    $query = $CI->db->query($sql,$idcmdp);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}
//Récupération détail de l'orientation du paier
function recuporientation($idor) {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_orientationpage a
    WHERE a.idor=".$idor; 
    $query = $CI->db->query($sql,$idor);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}
//Récupération propriété du paier
function recuppropriete($idpr) {
    $CI = & get_instance();
    $sql = "SELECT 	*
    FROM ipw_proprietepage a
    WHERE a.idpp=".$idpr; 
    $query = $CI->db->query($sql,$idpr);
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}
//Récupération du moyen de payement
function mp() {
     $CI = & get_instance();
     $where = "statusmp= 1"; // on récupère seullement les mp active
     $CI->db->select('*');
     $CI->db->from('ipw_moyen_payement');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}
 //get livraison infos ipw_commande
    function get_livraisoncmd($keyuniq) {
         $CI = & get_instance(); 
    $sql  = "SELECT  
               *
               
                FROM  ipw_cmd_livraison WHERE keyuniq = '".$keyuniq."'";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0) {
        return $query->row();
        } else {
        return false;
        }
    } 
    //get invoice 
    function getinvoice($idcmd) {
         $CI = & get_instance(); 
    $sql  = "SELECT 
    count(*) as total,  
               numfacture, titre, file_facture
               
                FROM  ipw_factures WHERE idcmd = '".$idcmd."'";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0) {
        return $query->row();
        } else {
        return false;
        }
    } 
	//Get all mission for order selected 
	//Récupération du moyen de payement
function mission_byorder($idorder) {
     $CI = & get_instance();
     $where = "idorder= ".$idorder; // on récupère seullement les mp active
     $CI->db->select('idm,titre,date_debut,date_fin');
     $CI->db->from('distri_missions');
     $CI->db->where($where);
     $query = $CI->db->get();
    $records = $query->result_array();
return $records;
}

function mission_byQuartier($codeIris, $cmdkey)
{
    //get idmission and title
    $CI = &get_instance();
    $CI->db->select('distri_missions.idm , distri_missions.titre');
    $CI->db->from('ipw_quartiercmd');
    $CI->db->join('ipw_commande', 'ipw_quartiercmd.cmdkey = ipw_commande.keyuniq');
    $CI->db->join('distri_missions', 'ipw_commande.idcmd = distri_missions.idorder');
    $CI->db->join('ipw_missionmapjson', 'ipw_missionmapjson.idmission = distri_missions.idm and ipw_missionmapjson.codeiris = ipw_quartiercmd.code_iris');
    $CI->db->where('ipw_quartiercmd.code_iris', $codeIris);
    $CI->db->where('ipw_quartiercmd.cmdkey', $cmdkey);
    $query = $CI->db->get();
    $records = $query->row();
    $idmission =  $records->idm;
    $titre =  $records->titre;

    $data = [
        'idmission' => $idmission,
        'titre' => $titre,
    ];

    return $data;
}