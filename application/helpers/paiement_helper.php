<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

function getsignature ($params,$key) 
{
 /**
 * Fonction qui calcule la signature.
 * $params : tableau contenant les champs à envoyer dans le formulaire.
 * $key : clé de TEST ou de PRODUCTION
 */
 //Initialisation de la variable qui contiendra la chaine à chiffrer
 $contenu_signature = "";
 //Tri des champs par ordre alphabétique
 //ksort($params);
 foreach($params as $nom=>$valeur){
 //Récupération des champs vads_
 if (substr($nom,0,5)=='vads_'){
 //Concaténation avec le séparateur "+"
 $contenu_signature .= $valeur."+";
 }
 }
 //Ajout de la clé en fin de chaine
 $contenu_signature .= $key;
 //Encodage base64 de la chaine chiffrée avec l'algorithme HMAC-SHA-256
 $signature = base64_encode(hash_hmac('sha256',$contenu_signature, $key, true));
 return $signature;
 } 


 ?>