<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_ad_pricelist extends MY_Model {

	private $primary_key 	= 'id_pricelist';
	private $table_name 	= 'ad_pricelist';
	private $field_search 	= ['name', 'id_currency', 'is_default'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
		 	'table_name' 	=> $this->table_name,
		 	'field_search' 	=> $this->field_search,
		 );

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "ad_pricelist.".$field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . "ad_pricelist.".$field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . "ad_pricelist.".$field . " LIKE '%" . $q . "%' )";
        }

		$this->join_avaiable();
        $this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "ad_pricelist.".$field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . "ad_pricelist.".$field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . "ad_pricelist.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
        	$this->db->select($select_field);
        }
		
		$this->join_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        $this->db->order_by('ad_pricelist.'.$this->primary_key, "DESC");
		$query = $this->db->get($this->table_name);

		return $query->result();
	}

	public function join_avaiable() {
		$this->db->join('ad_currency', 'ad_currency.id_currency = ad_pricelist.id_currency', 'LEFT');
	    
    	return $this;
	}


	public function getdefault()
    {

        $this->join_avaiable();
        $this->db->where("is_default",1);
        $this->db->order_by('ad_pricelist.'.$this->primary_key, "DESC");
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

}

/* End of file Model_ad_pricelist.php */
/* Location: ./application/models/Model_ad_pricelist.php */