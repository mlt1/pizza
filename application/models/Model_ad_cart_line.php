<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_ad_cart_line extends MY_Model
{

    private $primary_key = 'id_cartline';
    private $table_name = 'ad_cart_line';
    private $field_search = ['id_cart', 'id_product', 'sub_totoal'];

    public function __construct()
    {
        $config = array(
            'primary_key' => $this->primary_key,
            'table_name' => $this->table_name,
            'field_search' => $this->field_search,
        );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "ad_cart_line." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "ad_cart_line." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "ad_cart_line." . $field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "ad_cart_line." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "ad_cart_line." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "ad_cart_line." . $field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) and count($select_field)) {
            $this->db->select($select_field);
        }

        $this->join_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        $this->db->order_by('ad_cart_line.' . $this->primary_key, "DESC");
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable()
    {
        $this->db->join('ad_cart', 'ad_cart.id_cart = ad_cart_line.id_cart', 'LEFT');
        $this->db->join('ad_product', 'ad_product.id_product = ad_cart_line.id_product', 'LEFT');

        return $this;
    }


    public function findByProduct($product_id)
    {
        $cartline = $this->db->select('*')
            ->get_where($this->table_name, array('id_product' => $product_id))
            ->row();

        return $cartline;
        
    }


    public function update_cartline($cartline_id, $update_data)
    {

        $this->db->where('id_cartline  ', $cartline_id);
        return $this->db->update($this->table_name, $update_data);
    }

}

/* End of file Model_ad_cart_line.php */
/* Location: ./application/models/Model_ad_cart_line.php */